﻿using System.Linq;
using System.Threading;
using Dapper.Extension.AspNetCore;
using Fenglisu.Base.Caching;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Model;

namespace Cache
{
    public class DictionaryCache
    {
        private readonly IDapper _dapper;
        private readonly ICacheManager _cacheManager;

        public DictionaryCache(IDapper dapper, ICacheManager cacheManager)
        {
            _dapper = dapper;
            _cacheManager = cacheManager;
        }

        public void Load()
        {
            var sql = "SELECT * from Dictionary";
            var list = _dapper.Query<DictionaryModel>(sql).ToList();
            var d = list.ToDictionary(i => i.Id, i => new { Id = i.Value, Value = i.Value, Text = i.Text, Module = i.Module });
            _cacheManager.Set("DictionaryKey", d);
        }
    }
}
