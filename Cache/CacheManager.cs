﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Cache
{
    public static class CacheManager
    {
        public static IApplicationBuilder UseDataCaching(this IApplicationBuilder app)
        {
            var dictionary = app.ApplicationServices.GetRequiredService<DictionaryCache>();
            dictionary.Load();
            return app;
        }
    }
}
