﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class DictionaryModel
    {
        public int Id { get; set; }
        public string Module { get; set; }
        public int Sort { get; set; }
        public int Value { get; set; }
        public string Text { get; set; }
    }
}
