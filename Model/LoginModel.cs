﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class LoginRequest1
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class LoginResponse1
    {
        public string Token { get; set; }
    }
}
