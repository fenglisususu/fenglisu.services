﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class MenuItemModel
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public int Level { get; set; }
        public bool IsLeaf { get; set; }
        public bool Checked { get; set; }
        public string Icon { get; set; }
        public IList<MenuItemModel> Children { get; set; }

        public MenuItemModel()
        {
            Children = new List<MenuItemModel>();
        }
    }
}
