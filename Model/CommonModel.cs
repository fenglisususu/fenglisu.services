﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class UploadRequest
    {
        public string FileName { get; set; }
        public byte[] Data { get; set; }
        public string Key { get; set; }
    }
}
