﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class LoginRequest
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }

    public class LoginResponse
    {
        public Guid Id { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
    }

    public class GetUserPicsRequest
    {
        public Guid UserId { get; set; }
    }

    public class GetUserPicsResponse
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Picture { get; set; }
    }

    public class SaveUserPicsRequest
    {
        public Guid UserId { get; set; }
        public List<string> Pics { get; set; }
    }

    public class RemoveUserPicRequest
    {
        public Guid UserId { get; set; }
        public string Picture { get; set; }
    }
}
