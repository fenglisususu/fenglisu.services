﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class PokemonModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Attribute { get; set; }
    }
}
