﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Exceptions;
using Fenglisu.Base.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Services
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var ex = context.Exception;
            object response;
            if (ex is AspireException aspireException)
            {
                response = Result.OutputFail(aspireException.Message, aspireException.Code);
            }
            else
            {
                response = Result.OutputFail(ex.Message, "500");
            }

            context.Result = new JsonResult(response);
            context.ExceptionHandled = true;
        }
    }
}
