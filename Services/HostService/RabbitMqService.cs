﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Fenglisu.RabbitMq;
using Microsoft.Extensions.Options;

namespace Services.HostService
{
    public class RabbitMqService : HostService
    {
        private readonly RabbitMqBusiness _business;
        public RabbitMqService(IOptions<RabbitMqOptions> options, RabbitMqBusiness business) : base(options)
        {
            _business = business;
            base.QueueName = "quene1";
            base.RouteKey = "key1";
        }

        public override bool Process(string message)
        {
            var res = _business.SaveMessage(message);
            return res;
        }
    }
}
