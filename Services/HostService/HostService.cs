﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Fenglisu.RabbitMq;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace Services.HostService
{
    public class HostService : IHostedService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        protected string QueueName;
        protected string RouteKey;
        protected string ExchangeName = "exchangeName";
        public HostService(IOptions<RabbitMqOptions> options)
        {
            var factory = new ConnectionFactory()
            {
                HostName = options.Value.Host,
                UserName = options.Value.UserName,
                Password = options.Value.Password,
                Port = options.Value.Port,
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

        }

        private void Register()
        {
            _channel.ExchangeDeclare(ExchangeName, ExchangeType.Direct, true, false, null);
            _channel.QueueDeclare(QueueName, true, false, false, null);
            _channel.QueueBind(queue: QueueName, exchange: ExchangeName, routingKey: RouteKey);
            //每次只能从服务器接收一条信息,在未确认之前,不再接收发送信息
            _channel.BasicQos(0, 1, false);
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());
                var result = Process(message);
                if (result)
                {
                    _channel.BasicAck(ea.DeliveryTag, false);
                }
            };
            _channel.BasicConsume(queue: QueueName, consumer: consumer, autoAck: false);
        }

        private void DeRegister()
        {
            _connection.Close();
        }

        public virtual bool Process(string message)
        {
            throw new NotImplementedException();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Register();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            DeRegister();
            return Task.CompletedTask;
        }
    }
}
