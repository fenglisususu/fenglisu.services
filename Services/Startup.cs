using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Autofac;
using Microsoft.AspNetCore.Http;
using Business;
using Cache;
using Entity;
using Fenglisu.Consul;
using Fenglisu.EF;
using Fenglisu.Log;
using Fenglisu.RabbitMq;
using Fenglisu.Redis;
using Fenglisu.Web;
using Fenglisu.TencentCloud;
using Hangfire;
using Hangfire.Annotations;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Model;
using Services.HostService;

namespace Services
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication("Token").AddToken("Token");
            #region jwt认证方案
            //services.AddAuthentication(x =>
            //{
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddJwtBearer(options =>
            //{
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        //是否验证发行者
            //        ValidateIssuer = false,
            //        //将用于检查令牌的发行者是否与此发行者相同
            //        ValidIssuer = "liuchongzhi",
            //        //在令牌验证期间验证受众
            //        ValidateAudience = false,
            //        //检查令牌的受众群体是否与此受众群体相同
            //        ValidAudience = "liuchongzhi",
            //        //验证生命周期
            //        ValidateLifetime = true,
            //        //是否调用对签名securityToken的SecurityKey进行验证
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("123456123456123456"))
            //    };
            //    options.Events = new JwtBearerEvents
            //    {
            //        OnMessageReceived = context =>
            //        {
            //            return Task.CompletedTask;
            //        },
            //        OnTokenValidated = context =>
            //        {
            //            return Task.CompletedTask; ;
            //        },
            //        OnAuthenticationFailed = context =>
            //        {
            //            //Token expired
            //            if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
            //            {
            //                context.Response.Headers.Add("Token-Expired", "true");
            //            }
            //            return Task.CompletedTask;
            //        },
            //        OnForbidden = context =>
            //        {
            //            return Task.CompletedTask;
            //        },
            //        OnChallenge = context =>
            //        {
            //            return Task.CompletedTask;
            //        }
            //    };
            //});
            #endregion

            services.AddAuthorization(config =>
            {
                config.AddPolicy("LoginPolicy",
                    configPolicy =>
                    {
                        configPolicy.Requirements.Add(new LoginPolicyRequirement());
                    });
            });

            services.AddSingleton<IAuthorizationHandler, LoginPolicyHandler>();

            services.AddCors(options =>
             {
                 options.AddPolicy("MyCors", build =>
                     {
                         build.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                     });
             });

            services.AddControllers(c =>
            {
                c.Filters.Add(typeof(LoggingActionFilter));
            });
            //services.AddControllers(c =>
            //{
            //    c.Filters.Add<ExceptionFilter>();
            //});

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Services", Version = "v1" });
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<DictionaryCache>();

            // 添加log4net
            services.AddLog4NetLogger(options => { options.ConfigPath = "log4net.config"; });

            services.AddRedisCache(Configuration.GetSection("Redis"));

            // 添加Consul
            services.AddConsul(Configuration.GetSection("Consul"))
                // Consul服务注册与发现
                .AddConsulServiceRegistry(Configuration.GetSection("ServiceRegistry"));

            services.AddDbContext<FenglisuContext>(builder => builder.UseSqlServer(Configuration.GetConnectionString("Yun"),
                optionsBuilder => optionsBuilder.EnableRetryOnFailure()));
            services.AddEFRepository<FenglisuContext>();

            services.AddDapperForMySql(options =>
            {
                options.ConnectionString = Configuration.GetConnectionString("MySql");
            });

            services.AddRabbitMq(Configuration.GetSection("RabbitMq"));
            services.AddHostedService<RabbitMqService>();

            services.AddHangfire(configuration =>
            {
                configuration.UseSqlServerStorage(Configuration.GetConnectionString("Hangfire"));
            });

            services.AddQCloudObjectStorage(Configuration.GetSection("TencentCOS"));
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(UserBusiness).Assembly);
            //var container = builder.Build();
            //IocManager.Register(container);
            //IocManager.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // 健康监测中间件
            app.UseHealthCheck();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseSwagger();
                //app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Services v1"));
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Services v1"));

            //app.UseHttpsRedirection();

            app.UseRouting();

            //UseCors必须在这个位置
            app.UseCors("MyCors");

            app.UseTokenAuthentication();
            //app.UseAuthentication();

            app.UseAuthorization();

            // 在Consul中注册服务
            app.UseConsulServiceRegistry();

            app.UseDataCaching();

            //app.UseHangfireDashboard();
            app.UseHangfireServer();
            var business = app.ApplicationServices.GetRequiredService<HangfireBusiness>();
            RecurringJob.AddOrUpdate(() => business.TestHangfire(), "*/10 * * * * ?");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
            });
        }
    }
}
