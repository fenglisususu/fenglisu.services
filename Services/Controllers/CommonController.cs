﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Fenglisu.Base.Contexts;
using Fenglisu.Base.Cryptography;
using Fenglisu.Base.Exceptions;
using Fenglisu.Base.Models;
using Microsoft.AspNetCore.Authorization;
using Model;

namespace Services.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly CommonBusiness _commonBusiness;
        public CommonController(CommonBusiness commonBusiness)
        {
            _commonBusiness = commonBusiness;
        }

        [HttpPost]
        public Result<string> Upload(UploadRequest request)
        {
            return Result.OutputSuccess(_commonBusiness.Upload(request));
        }
    }
}
