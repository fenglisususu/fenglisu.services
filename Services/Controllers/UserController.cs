﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Fenglisu.Base.Models;
using Model;

namespace Services.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserBusiness _userBusiness;

        public UserController(UserBusiness userBusiness)
        {
            _userBusiness = userBusiness;
        }

        [HttpPost]
        public Result<LoginResponse> Login(LoginRequest request)
        {
            return Result.OutputSuccess(_userBusiness.Login(request));
        }

        [HttpPost]
        public Result<List<string>> GetUserPics(GetUserPicsRequest request)
        {
            return Result.OutputSuccess(_userBusiness.GetUserPics(request));
        }

        [HttpPost]
        public Result<bool> SaveUserPics(SaveUserPicsRequest request)
        {
            return Result.OutputSuccess(_userBusiness.SaveUserPics(request));
        }

        [HttpPost]
        public Result<bool> RemoveUserPic(RemoveUserPicRequest request)
        {
            return Result.OutputSuccess(_userBusiness.RemoveUserPic(request));
        }
    }
}
