﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Fenglisu.Base.Models;
using Model;

namespace Services.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly MenuBusiness _menuBusiness;

        public MenuController(MenuBusiness menuBusiness)
        {
            _menuBusiness = menuBusiness;
        }

        [HttpPost]
        public Result<List<MenuItemModel>> GetMenus()
        {
            return Result.OutputSuccess(_menuBusiness.GetMenus());
        }
    }
}
