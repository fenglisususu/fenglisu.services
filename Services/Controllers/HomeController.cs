﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using Business;
using Fenglisu.Base.Exceptions;
using Fenglisu.Base.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Model;
using Dapper;

namespace Services.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly HomeBusiness _homeBusiness;
        private readonly ILogger<HomeController> _log;
        private readonly IHttpContextAccessor _contextAccessor;
        public HomeController(HomeBusiness homeBusiness, ILogger<HomeController> log, IHttpContextAccessor contextAccessor)
        {
            _homeBusiness = homeBusiness;
            _log = log;
            _contextAccessor = contextAccessor;
        }

        [HttpPost]
        [AllowAnonymous]
        public Result<List<string>> TestApi([FromBody] Input input)
        {
            _log.LogInformation("进入了/api/home/testapi");
            var context = _contextAccessor;
            var headers = context.HttpContext.Request.Headers;
            foreach (var pair in headers)
            {
                var key = pair.Key;
                foreach (var value in pair.Value)
                {
                    var v = value;
                }
            }
            //Thread.Sleep(4000);
            var res = _homeBusiness.TestApi();
            return Result.OutputSuccess<List<string>>(res);
        }

        [HttpPost]
        [AllowAnonymous]
        public Result<LoginResponse1> Login(LoginRequest1 request)
        {
            var res = _homeBusiness.Login(request);
            return Result.OutputSuccess(res);
        }

        [HttpPost]
        [AllowAnonymous]
        public Result<LoginResponse1> Login_jwt(LoginRequest1 request)
        {
            if (string.IsNullOrEmpty(request.UserName)) throw new AspireException("请输入用户名");
            if (string.IsNullOrEmpty(request.Password)) throw new AspireException("请输入密码");
            string token = "";
            if (true)
            {
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Nbf,$"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}") ,
                    new Claim (JwtRegisteredClaimNames.Exp,$"{new DateTimeOffset(DateTime.Now.AddMinutes(30)).ToUnixTimeSeconds()}"),
                    new Claim(ClaimTypes.NameIdentifier, "liuchongzhi"),
                    new Claim("Role", "admin"),
                };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("123456123456123456"));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var jwtSecurityToken = new JwtSecurityToken(
                    issuer: "liuchongzhi", //颁发者
                    audience: "liuchongzhi",
                    notBefore: DateTime.Now,
                    expires: DateTime.Now.AddSeconds(60),//过期时间
                    signingCredentials: creds, //自定义参数
                    claims: claims);
                token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
            }

            return Result.OutputSuccess(new LoginResponse1() { Token = token });
        }

        [HttpPost]
        [Authorize("LoginPolicy")]
        public string Secret()
        {
            return "secret";
        }
    }

    public class Input
    {
        public string Name { get; set; }
    }
}
