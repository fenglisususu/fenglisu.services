﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Services
{
    public class LoginPolicyRequirement : IAuthorizationRequirement
    {

    }

    public class LoginPolicyHandler : AuthorizationHandler<LoginPolicyRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, LoginPolicyRequirement requirement)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                if (context.User.IsInRole("manager"))
                    context.Succeed(requirement);
                else
                    context.Fail();
            }
            else
            {
                context.Fail();
            }
            return Task.CompletedTask;
        }
    }
}
