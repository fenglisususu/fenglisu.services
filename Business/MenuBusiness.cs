﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using Fenglisu.Base.Repositories;
using Model;

namespace Business
{
    public class MenuBusiness
    {
        private readonly FenglisuContext _context;
        private readonly IRepository<Menu> _menuRepository;
        public MenuBusiness(IRepository<Menu> menuRepository, FenglisuContext context)
        {
            _menuRepository = menuRepository;
            _context = context;
        }

        public List<MenuItemModel> GetMenus()
        {
            var menus = _menuRepository.FindAll().ToList();
            var allLevelMenus = new List<Menu>();
            foreach (var menu in menus)
            {
                allLevelMenus.Add(menu);
                allLevelMenus.AddRange(GetParentMenus(menus, menu));
            }
            var menuModels = GetMenus(allLevelMenus);

            return menuModels;
        }

        private List<Menu> GetParentMenus(List<Menu> allMenus, Menu childMenu)
        {
            var parentMenus = new List<Menu>();
            if (!childMenu.ParentId.HasValue)
                return parentMenus;
            var parentMenu = allMenus.Find(i => i.Id == childMenu.ParentId);
            if (parentMenu != null)
            {
                parentMenus.Add(parentMenu);
                parentMenus.AddRange(GetParentMenus(allMenus, parentMenu));
            }

            return parentMenus;
        }

        private List<MenuItemModel> GetMenus(List<Menu> menus, Guid? pid = null, int level = 0, bool showLeaf = true)
        {
            List<MenuItemModel> levelMenuModels = new List<MenuItemModel>();
            var levelMenus = menus.FindAll(i => i.Level == level && i.ParentId == pid).OrderBy(i => i.Sort).ToList();
            if (levelMenus.Count == 0) return levelMenuModels;

            foreach (var levelMenu in levelMenus)
            {
                var levelMenuModel = new MenuItemModel
                {
                    Key = levelMenu.Id.ToString(),
                    Text = levelMenu.Name,
                    Title = levelMenu.Name,
                    Link = levelMenu.Url,
                    Level = levelMenu.Level,
                    Icon = levelMenu.Icon
                };
                var levelSubMenus = menus.FindAll(i => i.ParentId == levelMenu.Id).OrderBy(i => i.Sort).ToList();
                if (levelSubMenus.Count > 0)
                    levelMenuModel.Children = GetMenus(menus, levelMenu.Id, level + 1, showLeaf);
                else if (showLeaf)
                    levelMenuModel.IsLeaf = true;

                if (!levelMenuModels.Exists(i => i.Key == levelMenuModel.Key))
                    levelMenuModels.Add(levelMenuModel);
            }

            return levelMenuModels;
        }
    }
}
