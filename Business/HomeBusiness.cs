﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Extension.AspNetCore;
using Entity;
using Fenglisu.Base.Contexts;
using Fenglisu.Base.Exceptions;
using Model;

namespace Business
{
    public class HomeBusiness
    {
        private readonly UserContextManager _contextManager;
        private readonly FenglisuContext _context;
        private readonly IDapper _dapper;
        public HomeBusiness(UserContextManager contextManager, FenglisuContext context, IDapper dapper)
        {
            _contextManager = contextManager;
            _context = context;
            _dapper = dapper;
        }

        public LoginResponse1 Login(LoginRequest1 request)
        {
            if (string.IsNullOrEmpty(request.UserName)) throw new AspireException("请输入用户名");
            if (string.IsNullOrEmpty(request.Password)) throw new AspireException("请输入密码");
            if (true)
            {
                _contextManager.SetUserContext(Guid.NewGuid(), "fenglisu", "13360003837");
            }
            return new LoginResponse1() { Token = UserContext.Current.Token };
        }

        public List<string> TestApi()
        {
            //var sql = "INSERT INTO Pokemon (`Name`,attribute) VALUES (@name,@attribute)";
            //_dapper.Execute(sql, new PokemonModel() { Attribute = "多重鳞片", Name = "洛奇亚" });
            //var names = _context.Tests.Select(c => c.Name).ToList();
            //return names;

            return new List<string>() { "liuchongzhi" };
        }
    }
}
