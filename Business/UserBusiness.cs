﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using Fenglisu.Base.Exceptions;
using Fenglisu.Base.Repositories;
using Model;

namespace Business
{
    public class UserBusiness
    {
        private readonly FenglisuContext _context;
        private readonly IRepository<UserPicture> _userPriRepository;
        private readonly IRepository<User> _useRepository;

        public UserBusiness(FenglisuContext context, IRepository<User> useRepository, IRepository<UserPicture> userPriRepository)
        {
            _context = context;
            _useRepository = useRepository;
            _userPriRepository = userPriRepository;
        }

        public LoginResponse Login(LoginRequest request)
        {
            //var user = _context.Users.FirstOrDefault(c => c.Account == request.Account);
            var user = _useRepository.Find(c => c.Account == request.Account);
            if (user == null) throw new FenglisuException("找不到对象");
            if (user.Password != request.Password) throw new FenglisuException("密码错误");
            return new LoginResponse() { Id = user.Id, Account = user.Account, Name = user.Name };
        }

        public List<string> GetUserPics(GetUserPicsRequest request)
        {
            var user = _useRepository.Find(c => c.Id == request.UserId);
            if (user == null)
            {
                throw new FenglisuException("找不到用户");
            }

            var list = _userPriRepository.FindAll(c => c.UserId == user.Id).Select(c => c.Picture).ToList();
            return list;
        }

        public bool SaveUserPics(SaveUserPicsRequest request)
        {
            var user = _useRepository.Find(c => c.Id == request.UserId);
            if (user == null)
            {
                throw new FenglisuException("找不到用户");
            }

            var result = false;
            if (request.Pics != null && request.Pics.Count > 0)
            {
                List<UserPicture> userPictures = new List<UserPicture>();
                foreach (var pic in request.Pics)
                {
                    userPictures.Add(new UserPicture { Id = Guid.NewGuid(), UserId = user.Id, Picture = pic, CreationTime = DateTime.Now, ModifyTime = DateTime.Now });
                }

                result = _userPriRepository.Add(userPictures);
            }

            return result;
        }

        public bool RemoveUserPic(RemoveUserPicRequest request)
        {
            var pic = _userPriRepository.Find(c => c.UserId == request.UserId && c.Picture == request.Picture);
            if (pic == null) throw new FenglisuException("找不到图片！");
            return _userPriRepository.DeletePhysical(pic);
        }
    }
}
