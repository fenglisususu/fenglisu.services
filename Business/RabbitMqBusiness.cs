﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fenglisu.Base.Caching;

namespace Business
{
    public class RabbitMqBusiness
    {
        private readonly ICacheManager _cache;
        public RabbitMqBusiness(ICacheManager cache)
        {
            _cache = cache;
        }

        public bool SaveMessage(string message)
        {
            _cache.Set("RabbitMqMessage", message);
            return true;
        }
    }
}
