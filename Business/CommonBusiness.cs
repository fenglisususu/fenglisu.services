﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using COSXML;
using COSXML.Auth;
using COSXML.Model.Object;
using COSXML.Utils;
using Entity;
using Fenglisu.Base.Contexts;
using Fenglisu.Base.Exceptions;
using Fenglisu.Base.ObjectStorage;
using Fenglisu.Base.Repositories;
using Fenglisu.TencentCloud;
using Microsoft.Extensions.Options;
using Model;

namespace Business
{
    public class CommonBusiness
    {
        private readonly IObjectStorage _objectStorage;
        private readonly FenglisuContext _fenglisuContext;
        private readonly IRepository<UserPicture> _userPicRepository;
        public CommonBusiness(IObjectStorage objectStorage, FenglisuContext fenglisuContext, IRepository<UserPicture> userPicRepository)
        {
            _objectStorage = objectStorage;
            _userPicRepository = userPicRepository;
            _fenglisuContext = fenglisuContext;
        }

        public string Upload(UploadRequest request)
        {
            var result = _objectStorage.Upload(request.Key, request.Data);
            if (!string.IsNullOrEmpty(result))
            {
                var res = _userPicRepository.Add(new UserPicture()
                {
                    Id = Guid.NewGuid(),
                    UserId = UserContext.Current.Id,
                    Picture = result
                });
                if (!res) throw new FenglisuException("上传图片失败！");
            }
            else
            {
                throw new FenglisuException("上传图片失败！");
            }
            return result;
        }
    }
}
