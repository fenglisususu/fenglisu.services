﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fenglisu.Base.Caching;

namespace Business
{
    public class HangfireBusiness
    {
        private readonly ICacheManager _cache;
        public HangfireBusiness(ICacheManager cache)
        {
            _cache = cache;
        }

        public void TestHangfire()
        {
            _cache.Remove("hangfirekey");
            _cache.Set("hangfirekey", "test");
        }
    }
}
